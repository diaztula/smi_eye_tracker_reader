"""
Server process that reads eye data from the eye tracker and stores in a circular queue.
This implementation works for only one client.
Uses collections.deque to mainain a circular list
"""

eyeTracker = "SMI"

# ----------------- Importing modules -----------------
import socket
import sys
import os
import pickle
import time
import argparse
from collections import deque
import platform

canMoveMouse = False # Will be true of the server process can move the mouse (platform dependent)
osName = platform.system()
if osName == "Windows":
    try: 
        import win32api
        canMoveMouse = True
    except:
        pass
elif osName == "Linux":
    try: 
        from Xlib import X, display
        canMoveMouse = True
        disp = display.Display() 
        root = disp.screen().root
    except:
	print "Unable to load Xlib, mouse emulation wont work"
        pass

#sys.path.append("../EyeTracker") # Module that talks to the eye tracker

import EyeTrackerSync as et

def moveMouseWithGaze():
    """Move the cursor with the gaze.
    """
    global e
    [r, eyeD] = e.getEyeTrackerData()
    if r == 1:
        l = len(eyeD) - 1
        if args.binocular:
            x = (eyeD[l].left.x + eyeD[l].right.x)/2
            y = (eyeD[l].left.y + eyeD[l].right.y)/2
        else:
            x, y = eyeD[l].x, eyeD[l].y
        if osName == "Linux":
            root.warp_pointer(x, y)
            disp.sync()
        elif osName == "Windows":
            win32api.SetCursorPos( (int(x), int(y)) )

# Process the message received from the client
def processMsg(data, address):
    """Receives and process a message from the client and sends the return data via socket.

    Return value: 0 = success, 1 = error.
    """
    global e
    global sock
    global running
    global protocol
    global count
    
    if data == "calibrate":
        r = e.calibrateEyeTracker()
        if (r == 1):
            sock.sendto("success", address)
        else:
            sock.sendto("error_%i"%r, address)
            
    elif data == "saveCalibration":
        print "------------- Client request to save current calibration, requesting the file name ------------"
        sock.setblocking(1)
        sock.sendto("sendCalibFile", address)
        try:
            print "Waiting for file name..."
            calibFileName, addr = sock.recvfrom(size)
            print "File name received: ", calibFileName
        except:
            print "Exception occurred: ", sys.exc_info()[1]
        if not args.mouseEmu:
            r = e.saveCalibration(calibFileName)
        else:
            r = 1
        if r == 1:
            print "Calibration file saved in server with success"
            sock.sendto("success", addr)
        else:
            print "Could not save calibration file to server, return code:", r
            sock.sendto("error_%i"%r, addr)
        sock.setblocking(False)

    elif data == "loadCalibration":
        print "------------- Client request to load previously saved calibration, requesting the file name ------------"
        sock.setblocking(1)
        sock.sendto("sendCalibFile", address)
        try:
            print "Waiting for file name..."
            calibFileName, addr = sock.recvfrom(size)
            print "File name received: ", calibFileName
        except:
            print "Exception occurred: ", sys.exc_info()[1]
        if not args.mouseEmu:
            r = e.loadCalibration(calibFileName)
        else:
            r = 1
        if r == 1:
            print "Calibration file loaded with success"
            sock.sendto("success", addr)
        else:
            print "Could not load calibration file from server, SMI return code:", r
            sock.sendto("error_%i"%r, addr)
        sock.setblocking(False)

    elif data == "connect":
        r = e.connectEyeTracker()
        if (r == 1):
            sock.sendto(pickle.dumps("success", protocol), address)
        else:
            sock.sendto(pickle.dumps("error_%i"%r, protocol), address)

    elif data == "start":
        r = e.startEyeTracker()
        if (r == 1):
            sock.sendto(pickle.dumps("success", protocol), address)
            count = 0
        else:
            sock.sendto(pickle.dumps("error_%i"%r, protocol), address)

    elif data == "clear":
        r = e.clearEyeTrackerData()
        if (r == 1):
            sock.sendto(pickle.dumps("success", protocol), address)
            count = 0
        else:
            sock.sendto(pickle.dumps("error_%i"%r, protocol), address)

    elif data == "get_data":
        [r, eyeD] = e.getEyeTrackerData()
        if (r == 1):
            sock.sendto(pickle.dumps(eyeD, protocol), address)
            count += len(eyeD)
        else:
            sock.sendto(pickle.dumps("no_data", protocol), address)

    elif data == "stop":
        r = e.stopEyeTracker()
        if (r == 1):
            sock.sendto(pickle.dumps("success", protocol), address)
            print "Last client received ", count, "samples"
        else:
            sock.sendto(pickle.dumps("error_%i"%r, protocol), address)
            
    elif data == "exit":
        running = 0

if __name__ == '__main__':
    # Define server properties
    host = ''
    port = 9003
    size = 65536
    count = 0
    protocol = 1 # binary
    # Configure server socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((host, port))
    sock.setblocking(False)
    print "Server listening to client on port %i"%port 
     
    # Parse command line arguments
    parser = argparse.ArgumentParser(description = "Server process for reading data from eye tracker")
    parser.add_argument("--mouseEmu" , default = 0 , help = "1 -> emulate the eye tracker with the mouse (default is 0)")  # Emulate eye tracker with mouse
    parser.add_argument("--binocular", default = 1 , help = "1 -> return binocular eye data (default") # Return data from both eyes
    parser.add_argument("--eyeFile"  , default = "", help = "if set, file previously recorded from where to read eye data")
    # Adjust the width and height
    parser.add_argument("--scaleX"   , default = 1.0, help = "float to scale x position of the eye (default is 1)")
    parser.add_argument("--scaleY"   , default = 1.0, help = "float to scale y position of the eye (default is 1)")    
    # Move the cursor pointer?
    parser.add_argument("--moveMouse", default = 0  , help = "1 -> update mouse position with the latest eye sample (default is 0)")
    # Number of calibration points
    parser.add_argument("--calibPts" , default = 5  , help = "number of calibration points: 5 (default) or 9")
    # Number of calibration points
    parser.add_argument("--screen"   , default = 0  , help = "screen number where to calibrate the eye tracker (default 0)")
    # IP of the station running the eye tracker
    parser.add_argument("--remoteIp" , default = "10.0.0.2"  , help = "IP of the remote station running the eye tracker (default 10.0.0.2)")
    # local IP
    parser.add_argument("--localIp"  , default = "10.0.0.1"  , help = "local IP (default 10.0.0.1)")

    args = parser.parse_args()
    print "mouseEmu  = ", args.mouseEmu
    print "binocular = ", args.binocular
    print "eyeFile   = ", args.eyeFile
    print "calibPts  = ", args.calibPts
    print "screen    = ", args.screen
    
    if args.moveMouse and not canMoveMouse:
        print "Error: can not move the mouse pointer with the gaze data"
        if osName == "Windows":
            print "Package win32api is probably not installed"
        elif osName == "Linux":
            print "Package Xlib is probably not installed"
            print "You can install it with \"sudo apt-get install python-xlib\", aborting..."
        sys.exit(1)
    moveMouse = args.moveMouse
    
    if eyeTracker == "SMI":
        e = et.EyeTracker( args.mouseEmu, args.binocular, float(args.scaleX), float(args.scaleY), int(args.calibPts), int(args.screen), args.eyeFile, args.remoteIp, args.localIp )
    elif eyeTracker == "Haytham":
        e = et.EyeTracker( args.mouseEmu, args.binocular, args.eyeFile, float(args.scaleX), float(args.scaleY) )
    e.createEyeTracker()
    e.connectEyeTracker()
    
    running = 1
    while running:
        try:
            data, address = sock.recvfrom(size)
            processMsg(data, address)
        except:
            e.run()
            if moveMouse:
                moveMouseWithGaze()
                
#            print "Erro com o socket"
#        finally:
#    e.stopEyeTracker()
            
    sock.close()
