# Reading gaze samples from SMI remote eye tracker #

SMI remote eye trackers run on a dedicated PC and it is recommended by SMI not to install any software to avoid performance lost in gaze tracking. Hence experimental software usually runs on a separated computer and gaze samples are read through the network. 

SMI provides an API for Windows OS to read gaze samples, and also calibrate the eye tracker, etc. Nonetheless, when running an experiment or interface controlled by gaze, our application might need to synchronize with the vertical refresh of the monitor. Hence many gaze samples that are crucial to our experiment will be lost.

For example, consider an experiment that runs on a typical LCD display with a frequency of 60Hz. It is usual to synchronize our application with the refresh rate of the monitor, i.e. we wait 16.6 ms every time we paint the screen. A RED500 eye tracker works at 500Hz, i.e. it produces a gaze sample every 2 ms, but we will read samples at 60Hz.

To avoid this problem I created a simple program in Python. It is composed by a **server** process, and a **client** module. The server runs standalone and continuously read data from the SMI software (that is likely running in the SMI computer) and puts it in a circular queue. The client module is imported by an application, and provides a class to read information from the server process. 

NOTE: these scripts include code from the iViewX SDK provided by SMI.

### Installation ###

Simply copy the content of the repo to a folder, let call it *SMI* as an example. Then open the Windows shell and go to the folder *SMI*, and run:

```
#!python
python serverSync.py 
```

If there are no errors, then the server is already running!

To read data from this server, do something like this:

```
#!python

import sys
sys.path.append("SMI")
import client as Client # client is the module containing the EyeTrackerClient class
import time

# Create object instance
eyeTracker = Client.EyeTrackerClient()

# Initialize
eyeTracker.initEyeTrackerClient()

# Start eye tracker (server will begin to enqueue the gaze samples)
eyeTracker.startEyeTracker()

# Read 2000 samples
for i in xrange(2000):
    eyeData = eyeTracker.readDataFromEyeTracker()
    if len(eyeData) > 0: # Got any data?
        print "n. samples =", len(eyeData), ", last sample:  xLeft=", eyeData[len(eyeData)-1].left.x, " yLeft=", eyeData[len(eyeData)-1].left.y, " tstamp=", eyeData[len(eyeData)-1].left.tStamp
    # simulate some delay, usually when working with graphics
    # In a typical monitor (60Hz), each data read should contain between 6 and 8 gaze samples
    # for SMI RED500 gaze tracker
    time.sleep(1.0/60.0)

# At the end, stop eye tracker 
eyeTracker.stopEyeTracker()
```

Each element in the list *eyeData* has data from both eyes (left and right). Each eye has attributes *x*, *y*, *tStamp*, and *pupilDiam*.

The server works in Windows because it uses the iViewX API. There is no API for Linux, but I would like to implement one soon! The *serverSync* script has additional options, please run: 

```
#!python
python serverSync.py --help
```
to see all available options. Please set properly the parameters "remoteIp" (IP address of the remote station running the eye tracker, default 10.0.0.2) and "localIp" with IP address of the station running the serverSync script.

### Mouse emulation mode ###

The server can simulate the SMI eye tracker using the mouse, i.e. gaze position corresponds to the current mouse position. This will be useful when debugging your application without need to use the eye tracker. The module can be used in Linux in mouse emulation mode exclusively (please see dependencies above).

Another advantage of using these scripts is that your application is abstracted from the eye tracker details, because it calls functions from the class EyeTrackerClient and does not talk explicitly to the iViewX API. 

### Dependencies ###

* iViewX API
* win32api for Windows, or Xlib for Linux, to be able to run in mouse emulation mode.
* Standard python packages.

### Support ###

If you have any problem, doubt or suggestion please send email to [diaztula@ime.usp.br](mailto:diaztula@ime.usp.br). 
