import socket
import pickle
import sys
import time

class EyeTrackerClient():
    """This class implements a client that permits reading from the server process.
    """
    
    def initEyeTrackerClient(self):
        """Initializes the eye tracker client object.
        
        Returns: 0 if success, 1 otherwise.
        """
        # configure the client
        self.port = 9003
        self.host = 'localhost'
        self.size = 65536
        self.timeout = 8
        retCode = 1
        # initialize socket
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.sock.setblocking(2)
            # connect to ivewx
            self.sock.sendto("connect", (self.host, self.port))
            response = self.sock.recv(self.size)     
            msg = pickle.loads(response)
            if (msg == "success"):
                retCode = 0
                print "Connected to server"
            else:
                print "Cannot connect to server"
        except:
            print "Cannot connect to server (not running?)"
        finally:
            return retCode
    
    def readDataFromEyeTracker(self):
        """Reads data from the eye tracker server. It is expected to receive
        all samples between the last call and this one.
        
        Returns:
            List of gaze acquisitions.
        """
        try:
            eyeData = []
            self.sock.sendto("get_data", (self.host, self.port))
            response = self.sock.recv(self.size)   
            rawData =  pickle.loads(response)
            if isinstance(rawData, list): 
                eyeData = rawData
            else:
                print "No data received from server process"
        except socket.error:
            print "socket error while talking to server process"
        finally:    
            return eyeData
        
    def calibrateEyeTracker(self):
        """Triggers calibration of the eye tracker.
        
        Returns: 0 if success, 1 otherwise.
        """
        retCode = 1
        try:
            self.sock.sendto("calibrate", (self.host, self.port))
            response = self.sock.recv(self.size)     
            if (response == "success"):
                print "Calibration completed!"
                retCode = 0
            else:
                print "Calibration failed", retCode
        except socket.error:
            print "Socket error while initiating calibration"
        finally:    
            return retCode

    def saveCalibration(self, calibFileName):
        """Saves current calibration in the host running the SMI software, not
        out server process.
        
        Args: 
            - calibFileName: name of the file to save calibration.
        
        Returns: 0 success, 1 otherwise.
        """
        retCode = 1
        try:
            self.sock.sendto("saveCalibration", (self.host, self.port))
            response = self.sock.recv(self.size)     
            if response == "sendCalibFile":
                self.sock.sendto(calibFileName, (self.host, self.port))
                response = self.sock.recv(self.size)     
                if (response == "success"):
                    print "Calibration saved to %s"%calibFileName
                    retCode = 0
            if retCode != 0:
                print "Save calibration to file failed"
        except socket.error:
            print "Socket error while trying calibration"
        finally:    
            return retCode

    def loadCalibration(self, calibFileName):
        """Loads calibration previously saved from a file. This file
        must exist in the host running the SMI software.
        
        Args: 
            - calibFileName: name of the file to load.
        
        Returns: 0 success, 1 otherwise.
        """
        retCode = 1
        try:
            self.sock.sendto("loadCalibration", (self.host, self.port))
            response = self.sock.recv(self.size)     
            if response == "sendCalibFile":
                self.sock.sendto(calibFileName, (self.host, self.port))
                response = self.sock.recv(self.size)     
                if (response == "success"):
                    print "Calibration loaded from %s with success"%calibFileName
                    retCode = 0
            if retCode != 0:
                print "Load calibration file failed"
        except socket.error:
            print "Socket error while trying calibration"
        finally:    
            return retCode

    def startEyeTracker(self):
        """Tells the server process to start collecting gaze acquisitions
        and put them in the queue.
        
        Returns: 0 if success, 1 otherwise.
        """
        retCode = 1
        try:
            self.sock.sendto("start", (self.host, self.port))
            response = self.sock.recv(self.size)     
            msg = pickle.loads(response)
            if (msg == "success"):
                retCode = 0
        except socket.error:
            print "Socket error while starting the eye tracker"
        finally:    
            return retCode

    def stopEyeTracker(self):
        """Tells the server process to stop collecting gaze acquisitions.
        The queue is not cleared.
        
        Returns: 0 if success, 1 otherwise.
        """
        retCode = 1
        try:
            self.sock.sendto("stop", (self.host, self.port))
            response = self.sock.recv(self.size)     
            msg = pickle.loads(response)
            if (msg == "success"):
                retCode = 0
        except socket.error:
            print "Socket error while stopping the eye tracker"
        finally:
            return retCode

    def clearEyeTrackerData(self):
        """Tells the server process to clear all previous acquisitions.
        
        Returns: 0 if success, 1 otherwise.
        """
        retCode = 1
        try:
            self.sock.sendto("clear", (self.host, self.port))
            response = self.sock.recv(self.size)     
            msg = pickle.loads(response)
            if (msg == "success"):
                retCode = 0
        except socket.error:
            print "Socket error while clearing the queue"
            retCode = 1
        finally:    
            return retCode
            
