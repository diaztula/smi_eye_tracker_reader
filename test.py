import sys
import client as Client
import time

# Create object instance
eyeTracker = Client.EyeTrackerClient()

# Initialize
eyeTracker.initEyeTrackerClient()

# Start eye tracker (server will begin to enqueue the gaze samples)
eyeTracker.startEyeTracker()

# Read 2000 samples
for i in xrange(2000):
    eyeData = eyeTracker.readDataFromEyeTracker()
    if len(eyeData) > 0: # Got any data?
        print "n. samples =", len(eyeData), ", last sample:  xLeft=", eyeData[len(eyeData)-1].left.x, " yLeft=", eyeData[len(eyeData)-1].left.y, " left pupil diam: ", eyeData[len(eyeData)-1].left.pupilDiam, " tstamp=", eyeData[len(eyeData)-1].left.tStamp 
    # simulate some delay, usually when working with graphics
    # In a typical monitor (60Hz), each data read should contain between 6 and 8 gaze samples
    # for SMI RED500 gaze tracker
    time.sleep(1.0/60.0)

# At the end, stop eye tracker 
eyeTracker.stopEyeTracker()
