'''
Class to read eye tracking data from SMI eye tracker
'''
from multiprocessing import Array, Value
from collections import deque
import ctypes as c
import SMI as SMI
import time
import math
import os
import sys
import csv
import platform
# !!!!! FOR MOUSE EMULATION !!!!!
canReadMouse = False
osName = platform.system()
if osName == "Windows":
    try: 
        import win32api
        canReadMouse = True
    except:
        pass
elif osName == "Linux":
    try: 
        from Xlib import display
        canReadMouse = True
        root         = display.Display().screen().root
    except:
        pass

MAX_SAMPLE_SIZE = 500

class Eye(c.Structure):
    _fields_ = [("x", c.c_double), ("y", c.c_double), ("pupilDiam", c.c_double), ("tStamp", c.c_double)]
    
class BothEyes(c.Structure):    
    _fields_ = [("left", Eye), ("right", Eye)]

class EyeTracker():
    def __init__(self, mouseEmu, binocular, scaleX, scaleY, calibPts, screen, fileName, remoteIp, localIp):
        print "create eye tracker"
        self.mouseEmu  = mouseEmu
        self.binocular = binocular
        self.fileName  = fileName
        self.scaleX    = scaleX
        self.scaleY    = scaleY
        self.calibPts  = calibPts
        self.screen    = screen
        self.localIp   = localIp
        self.remoteIp  = remoteIp
        if fileName != "":
            print "Read eye data from file: ", fileName 
        elif mouseEmu:
            print "mouse emulation mode"
        if binocular:
            print "return binocular data"

    def createEyeTracker(self):
        print "Create eye tracking data"
        if self.binocular:
            self.eyeData = deque(maxlen = MAX_SAMPLE_SIZE) 
        else:
            self.eyeData = deque(maxlen = MAX_SAMPLE_SIZE) 
        self.eyeCount   = Value('i', 0)
        self.runningT   = Value('i', 0)
        # SMI objec to to handle eye tracker
        self.smi = SMI.SMI(self.calibPts, self.screen)

    def connectEyeTracker(self):
        if self.fileName != "":
            print "Opening eye data file: ", self.fileName
            try:
                f = open(self.fileName)
            except:
                print "Eye data file does not exists"
                sys.exit(1)
            self.file = csv.reader(f, delimiter=',')     
            if self.file == None:
                print "Unable to open eye data file"
                sys.exit(1)
            return 1
        elif self.mouseEmu:
            print "Mouse emulation mode activated"
            return 1
        elif self.fileName == "":
            print "Connect to eye tracker"
            return self.smi.connect("iViewXSDK_Python_SimpleExperiment.txt", self.remoteIp, self.localIp)
        
    def calibrateEyeTracker(self):
        if self.fileName == "" and not self.mouseEmu: 
            print "Calibrate eye tracker" 
            return self.smi.calibrate()
        else:
            return 1
        
    def loadCalibration(self, calibFileName):
        print "Load calibration from", calibFileName
        return self.smi.loadCalibration(calibFileName)
    
    def saveCalibration(self, calibFileName):
        print "Save calibration to", calibFileName
        return self.smi.saveCalibration(calibFileName)

    def startEyeTracker(self):
        print "start reading eye data"
        #self.clearEyeTrackerData()
        self.runningT.value = 1
        return 1
        
    def getEyeTrackerData(self):
        if len(self.eyeData) > 0:
            retCode = 1
        else:
            retCode = -1
        retList = list(self.eyeData)
        self.clearEyeTrackerData()
        return [retCode, retList]

    def stopEyeTracker(self):
        print "stop reading from eye tracker"
        self.runningT.value = 0
        return 1
    
    def getMousePos(self):
        x, y = -1, -1
        if canReadMouse:
            if osName == "Linux":
                pointer = root.query_pointer()
                data = pointer._data
                x, y = data["root_x"], data["root_y"]
            elif osName == "Windows":
                x, y = win32api.GetCursorPos()
        return x, y
        
    def clearEyeTrackerData(self):
        #print "Clear eye tracker data"
        self.eyeData.clear()
        return 1

    def run(self):
        retCode = -1
        if (self.runningT.value == 1):
            leftX = leftY = rightX = rightY = tStamp = 0
            leftPupil = rightPupil = 0.0
            # !!!!!! MOUSE EMULATION !!!!!!!
            if (self.mouseEmu):
                leftX, leftY = self.getMousePos()
                rightX = leftX
                rightY = leftY
                tStamp = time.time() * 10000 # simula decimas de milisegundos
                tStamp = c.c_double(math.floor(tStamp))
                leftPupil = 15.0
                rightPupil = 15.0
                retCode = 1 # simula a leitura feita com sucesso
                
            elif self.fileName != "": # Read eye data from a file
                try:
                    line       =  self.file.next()
                except:
                    print "Reached end of eye data file"
                    retCode = -1
                else:
                    tStamp     =  long(line[0])
                    leftX      =  int(float(line[1]))
                    leftY      =  int(float(line[2]))
                    leftPupil  = float(line[3]) 
                    rightX     =  int(float(line[4]))
                    rightY     =  int(float(line[5]))
                    rightPupil = float(line[6])
                    retCode    = 1
            else: # Read from eye tracker
                retCode, sample = self.smi.getBinocularData()
                # ------------------------------
                if (retCode == 1):
                    # calcula point of regard como sendo o ponto medio dos dois olhos
                    leftX  = sample.leftEye.gazeX
                    leftY  = sample.leftEye.gazeY
                    rightX = sample.rightEye.gazeX
                    rightY = sample.rightEye.gazeY
                    leftPupil  = sample.leftEye.diam
                    rightPupil = sample.rightEye.diam
                    tStamp = c.c_double(math.floor(sample.timestamp/100))

            if (retCode == 1):
                # Leu dados do olhar, agora devolve-os monocular ou binocular
                if self.binocular:
                    tmpData = BothEyes(Eye(leftX*self.scaleX, leftY*self.scaleY, leftPupil, tStamp), Eye(rightX*self.scaleX, rightY*self.scaleY, rightPupil, tStamp))
                else: 
                    x = (leftX + rightX) / 2 * self.scaleX
                    y = (leftY + rightY) / 2 * self.scaleY
                    meanPupil = (leftPupil + rightPupil)/2
                    tmpData = Eye(x, y, meanPupil, tStamp)

                #self.eyeData[self.eyeCount.value] = tmpData
                self.eyeData.append(tmpData)
                self.eyeCount.value += 1
        # Se em modo simulacao, espera um pouquinho
        if self.mouseEmu or self.fileName != "":
            time.sleep(1.0/500.0)
        return retCode

